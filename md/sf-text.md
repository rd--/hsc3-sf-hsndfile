# sf-text

Print sound file.
Implemented in `C` using [libsndfile](http://www.mega-nerd.com/libsndfile/).

~~~~
$ hsc3-sf-text N F 0 ~/sw/hsc3-sf/data/au/mc-4-16.au
nm=/home/rohan/sw/hsc3-sf/data/au/mc-4-16.au,nf=4,nc=16,sr=1
0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1
0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1
0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1
0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1
$
~~~~
