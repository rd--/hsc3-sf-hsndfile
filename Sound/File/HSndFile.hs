-- | Read and write sound files using hsndfile.
module Sound.File.HSndFile where

import Control.Monad {- base -}
import Data.Maybe {- base -}
import System.Directory {- directory -}
import Prelude hiding (read) {- base -}

import qualified Foreign {- base -}

import qualified Data.ByteString as ByteString {- bytestring -}

import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Sound.File.Decode as Decode {- hsc3-sf -}
import qualified Sound.File.Encode as Encode {- hsc3-sf -}

import qualified Sound.File.Sndfile as Sndfile {- hsndfile -}
import qualified Sound.File.Sndfile.Buffer.Vector as Sndfile.Vector {- hsndfile-vector -}

-- | Sound file meta data.
data Sf_Header = Sf_Header
  { channelCount :: Int
  , frameCount :: Int
  , sampleRate :: Double
  , format :: Sndfile.Format
  }
  deriving (Eq, Show)

-- | Duration of sound file in seconds.
duration :: Sf_Header -> Double
duration (Sf_Header _ nf sr _) = fromIntegral nf / sr

-- | Extract 'Sf_Header' from 'Sndfile.Info'.
info_to_header :: Sndfile.Info -> Sf_Header
info_to_header i =
  let nc = Sndfile.channels i
      nf = Sndfile.frames i
      sr = Sndfile.samplerate i
  in Sf_Header nc nf (fromIntegral sr) (Sndfile.format i)

-- | Generate 'Sndfile.Info' from 'Sf_Header'.
header_to_info :: Sf_Header -> Sndfile.Info
header_to_info (Sf_Header nc nf sr fmt) = Sndfile.Info nf (floor sr) nc fmt 1 True

-- | 'Sndfile.Format' for 32 bit floating point NeXT file.
fmt_au_f32_be :: Sndfile.Format
fmt_au_f32_be = Sndfile.Format Sndfile.HeaderFormatAu Sndfile.SampleFormatFloat Sndfile.EndianBig

{- | Read 'Sf_Header' of sound file.

> sf_header "non-existing-file"
-}
sf_header :: FilePath -> IO Sf_Header
sf_header fn = do
  -- the error message from Sndfile.openFile does not indicate the file name
  x <- doesFileExist fn
  when (not x) (error ("sf_header: file does not exist: " ++ fn))
  h <- Sndfile.openFile fn Sndfile.ReadMode Sndfile.defaultInfo
  let hdr = info_to_header (Sndfile.hInfo h)
  Sndfile.hClose h
  return hdr

read_buf :: (Sndfile.Sample e, Sndfile.Buffer t e) => FilePath -> IO (Sf_Header, Maybe (t e))
read_buf fn = do
  h <- Sndfile.openFile fn Sndfile.ReadMode Sndfile.defaultInfo
  let hd = info_to_header (Sndfile.hInfo h)
      Sf_Header nc nf _ _ = hd
      ns = nc * nf
  b <- Sndfile.hGetBuffer h ns
  return (hd, b)

-- | Read interleaved 'Vector.Vector' data.
read_vec :: Sndfile.Sample n => FilePath -> IO (Sf_Header, Maybe (Vector.Vector n))
read_vec fn = do
  (hd, b) <- read_buf fn
  case b of
    Just b' -> return (hd, Just (Sndfile.Vector.fromBuffer b'))
    Nothing -> return (hd, Nothing)

-- | Type specialised & erroring variant.
read_vec_f32 :: FilePath -> IO (Sf_Header, Vector.Vector Float)
read_vec_f32 = fmap (\(h, v) -> (h, fromMaybe (error "read_vec_f32") v)) . read_vec

-- | Type specialised & erroring variant.
read_vec_f64 :: FilePath -> IO (Sf_Header, Vector.Vector Double)
read_vec_f64 = fmap (\(h, v) -> (h, fromMaybe (error "read_vec_f64") v)) . read_vec

{- | Get ByteString for Float Vector

>>> Foreign.sizeOf (undefined :: Float) == 4
True
-}
vec_f32_bytestring :: Vector.Vector Float -> IO ByteString.ByteString
vec_f32_bytestring vec = do
  (foreignPtr, ix, cnt) <- Sndfile.toForeignPtr (Sndfile.Vector.toBuffer vec)
  let fromPtr ptr = ByteString.packCStringLen (Foreign.castPtr ptr, cnt * 4)
  byteString <- Foreign.withForeignPtr foreignPtr fromPtr
  when (ix /= 0) (error "vec_f32_bytestring: index not zero")
  when (cnt /= Vector.length vec) (error ("vec_f32_bytestring: count mismatch: " ++ show (cnt, Vector.length vec)))
  return byteString

{- | Read interleaved Float data as ByteString.

>>> (hd, bytes) <- read_bytestring_f32 "/home/rohan/data/audio/floating_1.wav"
>>> frameCount hd == (ByteString.length bytes `div` 4)
True
-}
read_bytestring_f32 :: FilePath -> IO (Sf_Header, ByteString.ByteString)
read_bytestring_f32 fn = do
  (hd, vec) <- read_vec_f32 fn
  byteString <- vec_f32_bytestring vec
  return (hd, byteString)

-- | Read 'Sf_Header' and (interleaved) audio channel data from sound file.
read_interleaved :: FilePath -> IO (Sf_Header, [Double])
read_interleaved fn = do
  (hd, v) <- read_vec fn
  let f = Vector.toList
  return (hd, maybe [] f v)

-- | Read 'Sf_Header' and (de-interleaved) audio channel data from sound file.
read :: FilePath -> IO (Sf_Header, [[Double]])
read fn = do
  (hd, dat) <- read_interleaved fn
  return (hd, Decode.deinterleave (channelCount hd) dat)

-- | Write 'Vector.Vector' data (interleaved).
write_vec :: Sndfile.Sample n => FilePath -> Sf_Header -> Vector.Vector n -> IO ()
write_vec fn hdr v = do
  let i = header_to_info hdr
  when (not (Sndfile.checkFormat i)) (error ("write_vec: illegal info: " ++ show i))
  r <- Sndfile.writeFile i fn (Sndfile.Vector.toBuffer v)
  when (r /= frameCount hdr) (error "write_vec")

-- | Variant with interleaved list input.
write_interleaved :: FilePath -> Sf_Header -> [Double] -> IO ()
write_interleaved fn hdr l = write_vec fn hdr (Vector.fromList l)

-- | Variant with deinterleaved list input.
write :: FilePath -> Sf_Header -> [[Double]] -> IO ()
write fn hdr l = do
  when (channelCount hdr /= length l) (error "write: channel mismatch")
  write_interleaved fn hdr (Encode.interleave l)
