# sf-extract

Extract section of audio file, resample as required..
Implemented in `C` using [libsndfile](http://www.mega-nerd.com/libsndfile/).

~~~~
hsc3-sf-extract input-file channel-count start-ix end-ix output-frame-count output-file
~~~~

Open `input-file`, which must have `channel-count` channels, read the
signal from the frame `start-ix` through to the frame `end-ix`,
if required resample the signal to have `output-frame-count` frames, write the
result to `output-file`.
