#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h> /* libsndfile */
#include <samplerate.h> /* libsamplerate */

#define die(...) fprintf(stderr,__VA_ARGS__); exit(1)

void *xmalloc(size_t size)
{
  void *p = malloc(size);
  if(p == NULL) {
    die("malloc() failed: %ld\n", (long)size);
  }
  return p;
}

int main(int argc, char **argv)
{
    if (argc != 7) {
        die("sf-extract input-file channel-count start-ix end-ix output-frame-count output-file\n");
    }
    char *in_fn = argv[1];
    long nc = atol(argv[2]);
    long start_ix = atol(argv[3]);
    long end_ix = atol(argv[4]);
    long out_nf = atol(argv[5]);
    char *out_fn = argv[6];

    long in_nf = end_ix - start_ix + 1;
    double src_ratio = (double)out_nf / (double)in_nf;
    printf("in_nf=%ld,src_ratio=%f\n", in_nf, src_ratio);

    /* SHARED I/O */
    SNDFILE *sf;
    SF_INFO hdr;
    float *in_d = xmalloc(in_nf * nc * sizeof(float));
    float *out_d = xmalloc(out_nf * nc * sizeof(float));

    /* READ */
    memset(&hdr, 0, sizeof(hdr));
    sf = sf_open(in_fn, SFM_READ, &hdr);
    if (sf == NULL) {
        die("sf_open failed: %s", sf_strerror(NULL));
    }
    if (nc != hdr.channels) {
        die("channel mismatch: %d", hdr.channels);
    }
    if (sf_seek(sf, start_ix, SEEK_SET) == -1) {
        die("sf_seek failed");
    }
    if (sf_readf_float(sf, in_d, in_nf) != in_nf) {
        die("sf_readf_float failed");
    }
    sf_close(sf);

    /* SAMPLE-RATE CONVERSION */
    if (out_nf == in_nf) {
        memcpy(out_d,in_d,out_nf * nc * sizeof(float));
    } else {
        SRC_DATA src = {in_d, out_d, in_nf, out_nf, 0, 0, 0, src_ratio};
        if (src_simple(&src, SRC_SINC_BEST_QUALITY, nc) < 0) {
            die("src_simple failed");
        }
    }

    /* WRITE */
    sf = sf_open(out_fn, SFM_WRITE, &hdr);
    if (sf == NULL) {
        die("sf_open failed: %s", sf_strerror(NULL));
    }
    if (sf_writef_float(sf, out_d, out_nf) != out_nf) {
        die("sf_writef_float failed");
    }
    sf_close(sf);

    free(in_d);
    free(out_d);
    return 0;
}
