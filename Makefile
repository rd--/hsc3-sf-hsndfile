all:
	echo "hsc3-sf-hsndfile"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hsc3-sf-hsndfile

push-tags:
	r.gitlab-push.sh hsc3-sf-hsndfile --tags

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
