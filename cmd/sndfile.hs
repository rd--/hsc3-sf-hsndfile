import Data.Bits {- base -}
import Data.List {- base -}
import Numeric {- base -}
import System.Environment {- base -}
import System.FilePath {- base -}
import Text.Printf {- base -}

import Data.List.Split {- split -}
import qualified Data.Vector.Storable as V {- vector -}

import qualified Sound.File.HSndFile as Sf {- hsc3-sf-hsndfile -}
import qualified Sound.File.Vector as Vector {- hsc3-sf -}

-- * Util

-- | Maximum absolute value at vector, ie. fold of 'max' of map of 'abs'.
vec_max_abs_f32 :: V.Vector Float -> Float
vec_max_abs_f32 = V.foldl (\r x -> max r (abs x)) 0

-- | Normalise vector, ie. multiply by 'recip' of 'vec_max_abs_f32'.
vec_normalise_f32 :: V.Vector Float -> V.Vector Float
vec_normalise_f32 v =
  let m = recip (vec_max_abs_f32 v)
  in V.map (* m) v

{- | Sum /nc/ channel interleaved vector to mono.  The signal is
scaled by the inverse of the number of channels.
-}
vec_mix_to_mono_f32 :: Int -> V.Vector Float -> V.Vector Float
vec_mix_to_mono_f32 nc v =
  let nf = V.length v `div` nc
      m = 1 / fromIntegral nc
  in V.generate nf (\i -> sum (map (\c -> v V.! ((i * nc) + c)) [0 .. nc - 1]) * m)

-- > parse_int_list "1,0" == [1,0]
parse_int_list :: String -> [Int]
parse_int_list = map read . splitOn ","

bracket :: ([a], [a]) -> [a] -> [a]
bracket (x, y) l = concat [x, l, y]

real_pp :: Int -> Double -> String
real_pp k n = showFFloat (Just k) n ""

-- * Re-write

sf_rw_channels :: [Int] -> FilePath -> FilePath -> IO ()
sf_rw_channels ch in_fn out_fn = do
  (hdr, v) <- Sf.read_vec_f32 in_fn
  let nc = Sf.channelCount hdr
      v_ch = Vector.vec_deinterleave nc v
      v' = Vector.vec_interleave (map (v_ch !!) ch)
      hdr' = hdr {Sf.channelCount = length ch}
  Sf.write_vec out_fn hdr' v'

sf_rw_normalise :: FilePath -> FilePath -> IO ()
sf_rw_normalise in_fn out_fn = do
  (hdr, v) <- Sf.read_vec_f32 in_fn
  Sf.write_vec out_fn hdr (vec_normalise_f32 v)

sf_rw_mix :: FilePath -> FilePath -> IO ()
sf_rw_mix in_fn out_fn = do
  (hdr, v) <- Sf.read_vec_f32 in_fn
  let nc = Sf.channelCount hdr
      hdr' = hdr {Sf.channelCount = 1}
  Sf.write_vec out_fn hdr' (vec_mix_to_mono_f32 nc v)

-- * Print

data Mode = ALL Bool | C0 | META

parse_mode :: String -> Mode
parse_mode md =
  case md of
    "C" -> ALL False
    "F" -> ALL True
    "C0" -> C0
    "META" -> META
    _ -> error "parse_mode"

meta_pp :: (FilePath, Sf.Sf_Header) -> String
meta_pp (fn, Sf.Sf_Header nc nf sr _) = printf "nm=%s,nf=%d,nc=%d,sr=%.0f" fn nf nc sr

sf_print :: Mode -> Int -> FilePath -> IO ()
sf_print z k fn = do
  (hdr, d) <- Sf.read fn
  case z of
    META -> putStrLn (meta_pp (fn, hdr))
    C0 ->
      let f = bracket ("[", "]") . concat . intersperse "," . map (real_pp k)
      in putStrLn (f (d !! 0))
    ALL frame_order ->
      let d' = if frame_order then transpose d else d
          f = unlines . map (unwords . map (real_pp k))
      in putStrLn (meta_pp (fn, hdr)) >> putStr (f d')

{-
let fn = "/home/rohan/data/audio/pf-c5.aif"
let fn = "/home/rohan/sw/hsc3-sf/au/mc-4-16.au"
let fn = "/home/rohan/sw/hsc3-sf/au/mc-11-2048.au"
sfprint META 0 fn
sfprint C0 0 fn
sfprint (ALL False) 0 fn
sfprint (ALL True) 0 fn

for i in *.aiff ; do runhaskell sf-print.hs C0 5 $i > $i.json ; done
-}

-- * Analysis

sf_stat :: FilePath -> IO ()
sf_stat fn = do
  (hdr, v) <- Sf.read_vec_f32 fn
  let nf = Sf.frameCount hdr
      sr = Sf.sampleRate hdr
      nc = Sf.channelCount hdr
      v' = Vector.vec_deinterleave nc v
  print
    ( ("nframes", nf)
    , ("srate", sr)
    , ("dur", fromIntegral nf / sr)
    , ("nchan", nc)
    , ("minima", map (V.foldl1 min) v')
    , ("maxima", map (V.foldl1 max) v')
    )

{-
sf_stat "/home/rohan/data/audio/pf-c5.aif"
sf_stat "/home/rohan/data/audio/gebet.wav"
-}

sf_zero_crossings :: FilePath -> [Int] -> IO [Maybe Int]
sf_zero_crossings fn ix = do
  (_hdr, vec) <- Sf.read_vec_f32 fn
  return (map (Vector.vec_next_zc vec) ix)

-- * Index

sf_index_md_1 :: FilePath -> Sf.Sf_Header -> String
sf_index_md_1 fn hdr =
  let fsec_to_minsec = flip divMod 60 . (round :: Double -> Int)
      fn' = dropExtension (takeFileName fn)
      nf = fromIntegral (Sf.frameCount hdr)
      sr = Sf.sampleRate hdr
      (m, s) = fsec_to_minsec (nf / sr)
  in -- nc = Sf.channelCount hdr
     printf "[%s](%s)=%02d:%02d," fn' fn m s

sf_index_md :: [FilePath] -> IO ()
sf_index_md fn = do
  hdr <- mapM Sf.sf_header fn
  putStrLn (unlines (zipWith sf_index_md_1 fn hdr))

-- * Multi-channel

gen_dat :: Num n => Int -> Int -> [[n]]
gen_dat nf nc =
  let gen_ch n = map (fromIntegral . fromEnum . testBit n) [0 .. nf - 1]
  in map gen_ch [0 .. nc - 1]

gen_mc :: FilePath -> Int -> Int -> IO ()
gen_mc fn nf nc = do
  let hdr = Sf.Sf_Header nc nf 1 Sf.fmt_au_f32_be
  Sf.write fn hdr (gen_dat nf nc)

gen_fn :: FilePath -> String -> Int -> Int -> FilePath
gen_fn dir ty n m = concat [dir, "/mc-", show n, "-", show m, ".", ty]

gen_mc_set :: FilePath -> [Int] -> IO ()
gen_mc_set dir =
  let g n = let m = 2 ^ n in gen_mc (gen_fn dir "au" n m) n m
  in mapM_ g

-- * Main

usage :: [String]
usage =
  [ "hsc3-sndfile"
  , ""
  , "  analysis stat file-name"
  , "  analysis next-zc file-name ix..."
  , ""
  , "  index md file-name..."
  , ""
  , "  multi-channel directory n:int m:int"
  , ""
  , "  print {C|F|C0|META} precision:int file-name..."
  , "    C = channel order"
  , "    F = frame order"
  , "    C0 = print only channel zero, in bracket notation, no header"
  , "    META = print only meta data"
  , ""
  , "  rw channels channel-list input-file output-file"
  , "  rw mix input-file output-file"
  , "  rw normalise input-file output-file"
  , "    channel-list = comma separated list of channels"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["analysis", "stat", fn] -> sf_stat fn
    "analysis" : "next-zc" : fn : ix ->
      sf_zero_crossings fn (map read ix)
        >>= mapM_ (putStrLn . maybe "EOF" show)
    "index" : "md" : fn -> sf_index_md fn
    ["multi-channel", dir, n, m] -> gen_mc_set dir [read n .. read m]
    "print" : md : k : fn -> mapM_ (sf_print (parse_mode md) (read k)) fn
    ["rw", "channels", ch, in_fn, out_fn] -> sf_rw_channels (parse_int_list ch) in_fn out_fn
    ["rw", "mix", ifn, ofn] -> sf_rw_mix ifn ofn
    ["rw", "normalise", ifn, ofn] -> sf_rw_normalise ifn ofn
    _ -> putStrLn (unlines usage)

{-
f1 = "/home/rohan/data/audio/gebet.wav"
f2 = "/home/rohan/data/audio/gebet.RL.wav"
sf_channels [1,0] f1 f2
-}
