/* hsc3-sf-text C 0 ~/sw/hsc3-sf/data/au/mc-11-2048.au | less */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#define die(...) fprintf(stderr,__VA_ARGS__); exit(1)

void sf_text(long nf, long nc, bool c_order, int f_prec, float *d)
{
    long w = c_order ? nf : nc;
    long h = c_order ? nc : nf;
    for (long i = 0; i < h; i++) {
        for (long j = 0; j < w; j++) {
            long k = c_order ? j * nc + i : i * nc + j;
            printf("%.*f%c", f_prec, d[k], j < w - 1 ? ' ' : '\n');
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 5) {
        die("sf-text {N|R} {F|C} precision:int file-name\n N = normalise A = actual\n F = frame order, C = channel order\n");
    }
    bool nrm = argv[1][0] == 'N';
    bool c_order = argv[2][0] == 'C';
    int f_prec = atoi(argv[3]);
    char *fn = argv[4];
    SF_INFO hdr;
    memset(&hdr, 0, sizeof(hdr));
    SNDFILE *sf = sf_open(fn, SFM_READ, &hdr);
    if (sf == NULL) {
        die("sf_open failed: %s", sf_strerror(NULL));
    }
    sf_command (sf, SFC_SET_NORM_FLOAT, NULL, nrm);
    long nf = hdr.frames;
    long nc = hdr.channels;
    printf("nm=%s,nf=%ld,nc=%ld,sr=%d\n", fn, nf, nc, hdr.samplerate);
    float *d = malloc(nf * nc * sizeof(float));
    if (d == NULL) {
        die("malloc failed");
    }
    long r = sf_readf_float(sf, d, nf);
    if (r != nf) {
        die("sf_readf_float failed: %ld != %ld", r, nf);
    }
    sf_text(nf, nc, c_order, f_prec, d);
    free(d);
    sf_close(sf);
    return 0;
}
