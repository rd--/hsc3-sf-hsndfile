hsc3-sf-hsndfile
----------------

[haskell][hs] soundfile ([hsc3-sf][hsc3-sf]) interface to
[hsndfile][hsf]

[hs]: http://haskell.org/
[hsc3-sf]: http://rohandrape.net/?t=hsc3-sf
[hsf]: http://code.haskell.org/hsndfile

# cli

[sf-extract](?t=hsc3-sf-hsndfile&e=md/sf-extract.md),
[sf-text](?t=hsc3-sf-hsndfile&e=md/sf-text.md),
[sndfile](?t=hsc3-sf-hsndfile&e=md/sndfile.md)

© [rohan drape][rd], 2010-2024, [gpl]

[rd]: http://rohandrape.net/
[gpl]: http://gnu.org/copyleft/

* * *

```
$ make doctest
Examples: 3  Tried: 3  Errors: 0  Failures: 0
$
```
