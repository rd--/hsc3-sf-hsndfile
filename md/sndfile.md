# hsc3-sndfile

## analysis stat

Print trivial sound file statistics.

~~~~
$ hsc3-sndfile analysis stat ~/data/audio/gebet.wav
(("nframes",1620335)
,("srate",44100.0)
,("dur",36.742290249433104)
,("nchan",2)
,("minima",[-0.511505126953125,-0.5872802734375])
,("maxima",[0.427978515625,0.578765869140625]))
$
~~~~

## analysis zc

For each indicated index, print the first subsequent zero-crossing.

~~~~
$ hsc3-sndfile analysis zc ~/data/audio/instr/farfisa/aad/flute8.flac 332000 580000
332050
580186
$
~~~~

## index

Write index file for named sound files.

~~~~
$ hsc3-sndfile index md uc/invisible/places/flac/*.flac
[A.0](uc/invisible/places/flac/A.0.flac)=02:30,
[A.1](uc/invisible/places/flac/A.1.flac)=03:32,
[B.0](uc/invisible/places/flac/B.0.flac)=01:30,
[C.0](uc/invisible/places/flac/C.0.flac)=02:20,
[C.1](uc/invisible/places/flac/C.1.flac)=01:12,
[places.miuc](uc/invisible/places/flac/places.miuc.flac)=17:13,
$
~~~~

## multi-channel

hsndfile equivalent to [multi-channel](?t=hsc3-sf&e=md/sf.md).

Writes 32-bit Float NeXT/AU files.

There is presently a limit at libsndfile to the number of channels that may be written.

~~~~
$ hsc3-sndfile multi-channel au ~/sw/hsc3-sf-hsndfile/data/au 2 10
$ ls -sS1 ~/sw/hsc3-sf-hsndfile/data/au
total 100
44 mc-10-1024.au
20 mc-9-512.au
12 mc-8-256.au
 4 mc-7-128.au
 4 mc-6-64.au
 4 mc-5-32.au
 4 mc-4-16.au
 4 mc-3-8.au
 4 mc-2-4.au
$ hsc3-au-print cs ro bw ~/sw/hsc3-sf-hsndfile/data/au/mc-3-8.au
nm="/home/rohan/sw/hsc3-sf/data/au/mc-3-8.au", nc=8, nf=3, sr=1, co=f
0 0 0
1 0 0
0 1 0
1 1 0
0 0 1
1 0 1
0 1 1
1 1 1
$
~~~~

## print

Print sound file, implemented in haskell using
[hsc3-sf-hsndfile](?t=hsc3-sf-hsndfile), which uses the
[haskell](http://www.haskell.org/) bindings to
[libsndfile](http://www.mega-nerd.com/libsndfile/).

~~~~
$ hsc3-sndfile print F 0 ~/sw/hsc3-sf/data/au/mc-4-16.au
nm=/home/rohan/sw/hsc3-sf/data/au/mc-4-16.au,nf=4,nc=16,sr=1
0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1
0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1
0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1
0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1
$
~~~~

## rw channels

Rewrite sound file with only indicated channels in indicated order.

~~~~
$ hsc3-sndfile rw channels 1,0 ~/data/audio/gebet.wav ~/data/audio/gebet.RL.wav
~~~~

## rw mix

Mix all channels to mono.  Output is scaled by the inverse of the number of channels.

~~~~
$ hsc3-sndfile rw mix ~/data/audio/gebet.wav ~/data/audio/gebet.mono.wav
~~~~

## rw normalise

Normalise sound file.
